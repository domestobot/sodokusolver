import numpy as np
from GUI import Grid
from random import shuffle,randint

class Sodoku():
    num_list = [1,2,3,4,5,6,7,8,9]

    soln_count = 0

    grid_default = [[5,3,0,0,7,0,0,0,0],
                    [6,0,0,1,9,5,0,0,0],
                    [0,9,8,0,0,0,0,6,0],
                    [8,0,0,0,6,0,0,0,3],
                    [4,0,0,8,0,3,0,0,1],
                    [7,0,0,0,2,0,0,0,6],
                    [0,6,0,0,0,0,2,8,0],
                    [0,0,0,4,1,9,0,0,5],
                    [0,0,0,0,8,0,0,0,0]]

    grid = grid_default

    def reset_count(self):
        self.soln_count = 0

    def possible(self, y, x, n):
        for i in range(9):
            if (self.grid[y][i] == n or
                self.grid[i][x] == n ):
                return False
        x0 = (x//3)*3
        y0 = (y//3)*3
        for i in range(3):
            for j in range(3):
                if(self.grid[y0+i][x0+j] == n):
                    return False
        return True

    def solve(self, max_soln = 1):
        # print(str(self.soln_count))
        if self.soln_count < max_soln:
            for y in range(9):
                for x in range(9):
                    if self.grid[y][x] == 0:
                        for n in self.num_list:
                            if self.possible(y, x, n):
                                self.grid[y][x] = n
                                self.solve(max_soln)
                                if self.soln_count < max_soln:
                                    self.grid[y][x] = 0
                        return
            self.soln_count += 1
        else:
            pass

    def animated_solve(self, game_board):
        pass

    def make_grid(self):
        shuffle(self.num_list)
        for x in range(9):
            for y in range(9):
                self.grid[y][x] = 0
            self.grid[0][x] = self.num_list[x]
        self.reset_count()
        self.solve()
        self.reset_count()
    
    def make_puzzle(self, difficulty=14):
        valid = {10, 11, 12, 13, 14, 15, 16, 17}
        if difficulty not in valid:
            raise ValueError(f"results: status must be one of {valid}.")
        empty_count = 0
        while empty_count < difficulty:
            x = randint(0,8)
            y = randint(0,8)
            if not self.grid[y][x] == 0:
                self.grid[y][x] = 0
                empty_count += 1
        self.reset_count()
    

    def print(self):
        print(np.matrix(self.grid))

    def set_grid(self, grid = grid_default):
        self.grid = grid

    def set_number(self, pos_y, pos_x, value):
        if self.possible(pos_y, pos_x, value):
            self.grid[pos_y][pos_x] = value
            return True
        else:
            return False

    def is_solved(self):
        for line in self.grid:
            for val in line:
                if val == 0:
                    return False
        
        return True

# sod = Sodoku()
# print('1current puzzle:')
# sod.print()
# sod.reset_count()
# sod.solve()
# print('1solution:')
# sod.print()
# sod.make_grid()
# print('2new solved grid:')
# sod.print()
# sod.make_puzzle()
# print('2new puzzle grid:')
# sod.print()
# sod.reset_count()
# sod.solve()
# print('2solution from solver:')
# sod.print()
# sod.make_grid()
# print('3new solved grid:')
# sod.print()
# sod.make_puzzle()
# print('3new puzzle grid:')
# sod.print()
# sod.reset_count()
# sod.solve()
# print('3solution from solver:')
# sod.print()
# sod.make_grid()
# print('4new solved grid:')
# sod.print()
# sod.make_puzzle()
# print('4new puzzle grid:')
# sod.print()
# sod.reset_count()
# sod.solve()
# print('4solution from solver:')
# sod.print()
# print("done")