import pygame
import time
import logging
import Sodoku
pygame.font.init()

class Box():

    def __init__(self, x_pos, y_pos, x_size, y_size, win, win_bound_x, win_bound_y, col_idle = (0,0,0)):
        self.x_pos = x_pos
        self.y_pos = y_pos
        
        if x_pos + x_size < win_bound_x + 1:
            self.x_size = x_size
        else:
            self.x_size = 2*x_size + x_pos - win_bound_x
            logging.warning("button size would exceed window limits and has been truncated on x_axis")
        if y_pos + y_size < win_bound_y + 1:
            self.y_size = y_size
        else:
            self.y_size = 2*y_size + y_pos - win_bound_y
            logging.warning("button size would exceed window limits and has been truncated on y_axis")

        self.win = win

        self.col_idle = col_idle
        
        self.state = 0

    def draw(self, col = (0,0,0), thick = 1):
            pygame.draw.rect(self.win, col, ((self.x_pos, self.y_pos), (self.x_size, self.y_size)), thick)

class Button(Box):

    def __init__(self, x_pos, y_pos, x_size, y_size, win, win_bound_x, win_bound_y, label, col_idle = (0,0,0), col_hover = (255,0,0), col_click = (0,255,0)):
        super().__init__(x_pos, y_pos, x_size, y_size, win, win_bound_x, win_bound_y, col_idle = col_idle)

        self.col_hover = col_hover
        self.col_click = col_click

        self.state = 0
        self.label = label

    def draw(self, state = 0):
        fnt = pygame.font.SysFont("comicsans", 40)
        txt = fnt.render(self.label, 1, (0, 0, 0))
        self.win.blit(txt, (self.x_pos + (self.x_size - txt.get_width())/2, self.y_pos + (self.y_size - txt.get_height())/2)) # TODO: need to add dimension detection to prevente drawing txt off screen or outside of button

        if state == 0:
            super().draw(self.col_idle)
        elif state == 1:
            super().draw(self.col_hover)
        elif state == 2:
            super().draw(self.col_click)
        else:
            logging.error("button state out of bounds")
    
    def update_state(self, mouse_pos, mouse_click):
        x_pos, y_pos = mouse_pos
        if x_pos > self.x_pos and x_pos < self.x_pos + self.x_size and y_pos > self.y_pos and y_pos < self.y_pos + self.y_size:
            if mouse_click:
                state = 2
                return True
            else:
                state = 1
                return False
        else:
            state = 0
            return False

class Grid_Square(Box):
    
    def __init__(self, value, x_pos, y_pos, x_size, y_size, win, win_bound_x, win_bound_y, col_idle=(0,0,0), col_selected=(0,255,0)):
        super().__init__(x_pos, y_pos, x_size, y_size, win, win_bound_x, win_bound_y, col_idle=col_idle)

        self.value = value
        self.temp = 0

        self.col_selected = col_selected
        self.selected = False

        self.fnt = pygame.font.SysFont("comicsans", 40)
    
    def draw(self):
        if(self.temp != 0 and self.value == 0):
            txt = self.fnt.render(str(self.temp), 1, (128,128,128))
            self.win.blit(txt, (self.x_pos + 5, self.y_pos + 5))
        elif(self.value != 0):
            txt = self.fnt.render(str(self.value), 1, (0,0,0))
            self.win.blit(txt, (self.x_pos + (self.x_size - txt.get_width())/2, self.y_pos + (self.y_size - txt.get_height())/2))
        
        if self.selected:
            super().draw(col=self.col_selected, thick = 5)
        else:
            super().draw(col=self.col_idle, thick = 1)

    def draw_for_solver(self, green):
        fnt = pygame.font.SysFont("comicsans", 40)

        spacing = self.x_size/9

        pygame.draw.rect(self.win, (255, 255, 255), (self.x_pos, self.y_pos, self.x_size, self.y_size), 0)
        if self.value != 0:
            txt = fnt.render(str(self.value), 1, (0, 0, 0))
            self.win.blit(txt, (self.x_pos + (self.x_size - txt.get_width())/2, self.y_pos + (self.y_size - txt.get_height())/2))
        if green:
            pygame.draw.rect(self.win, (0, 255, 0), (self.x_pos, self.y_pos, self.x_size, self.y_size), 5)
        else:
            pygame.draw.rect(self.win, (255, 0, 0), (self.x_pos, self.y_pos, self.x_size, self.y_size), 5)
        
    def set_selected(self, selected = True):
        self.selected = selected

    def set_value(self, value):
        self.value = value

    def set_temp(self, value):
        self.temp = value

class Grid():

    def __init__(self, x_pos, y_pos, x_size, y_size, win, puzzle):
        self.rows = 9
        self.cols = 9

        self.x_pos = x_pos
        self.y_pos = y_pos
        self.x_size = x_size
        self.y_size = y_size

        self.win = win
        
        self.puzzle = puzzle
        self.board = [[Grid_Square(puzzle.grid[i][j], x_pos + i*x_size/self.rows, y_pos + j*x_size/self.cols, x_size/self.rows, y_size/self.cols, self.win, 540, 660) for j in range(self.cols)] for i in range(self.rows)]
        self.selected = None
        self.finished = False # TODO: implement is finished checks to disable grids interactivity when current puzzle is complete

    def draw(self):
        x_space = self.x_size/self.rows
        y_space = self.y_size/self.cols


        for i in range(self.rows+1):
            if i % 3 == 0:
                thick = 4
            else:
                thick = 1
            pygame.draw.line(self.win, (0,0,0), (self.x_pos, i*y_space), (self.x_pos + self.x_size, i*y_space), thick)
            pygame.draw.line(self.win, (0,0,0), (i*x_space, self.y_pos), (i*x_space, self.y_pos + self.y_size), thick)
        
        for i in range(self.rows):
            for j in range(self.cols):
                self.board[i][j].draw()

    def get_square_at_pos(self, pos):
        x_pos, y_pos = pos
        if x_pos < (self.x_size + self.x_pos) and y_pos < (self.y_pos + self.y_size):
            spacing = self.x_size / 9
            x = (x_pos - self.x_size) // spacing
            y = (y_pos - self.y_size) // spacing
            return (int(y), int(x))
        else:
            return None

    def select_square(self, pos):
        row, col = pos
        if self.selected:
            old_row, old_col = self.selected
            self.board[old_col][old_row].selected = False
        
        self.board[col][row].selected = True
        self.selected = (row, col)

    def clear_selected(self):
        row, col = self.selected
        if self.board[col][row].value == 0:
            self.board[col][row].set_temp(0)

    def set_value_at(self, col, row, value):
        if self.puzzle.set_number(col, row, value):
            self.board[col][row].set_value(value)
            return True
        else:
            return False

    def new_game(self):
        self.puzzle.reset_count()
        self.puzzle.make_grid()
        self.puzzle.make_puzzle()
        
        self.board = [[Grid_Square(self.puzzle.grid[i][j], self.x_pos + i*self.x_size/self.rows, self.y_pos + j*self.x_size/self.cols, self.x_size/self.rows, self.y_size/self.cols, self.win, 540, 660) for j in range(self.cols)] for i in range(self.rows)]
        self.selected = None
        self.finished = False

    def animated_solve(self):
        # TODO: would require some kind of event handler to properly decouple business logic from GUI
        if not self.finished:
            for y in range(9):
                for x in range(9):
                    if self.puzzle.grid[y][x] == 0:
                        for n in self.puzzle.num_list:
                            if self.puzzle.possible(y, x, n):
                                self.puzzle.grid[y][x] = n
                                self.board[y][x].set_value(n)

                                self.board[y][x].draw_for_solver(True)
                                pygame.display.update()
                                pygame.time.delay(100)

                                self.animated_solve()

                                if not self.finished:
                                    self.puzzle.grid[y][x] = 0
                                    self.board[y][x].set_value(0)
                                    self.board[y][x].draw_for_solver(False)
                                    pygame.display.update()
                                    pygame.time.delay(100)
                        return
            self.finished = True

    def set_selected_temp(self, value):
        row, col = self.selected
        self.board[col][row].set_temp(value)

    def set_finished(self, status):
        self.finished = status

class TimeStamp():
    
    def __init__(self):
        self.start = time.time()
        
    def get_playtime(self):
        return round(time.time() - self.start)

    def reset(self):
        self.start = time.time()

    def get_timestring(self):
        game_time = self.get_playtime()
        secs = game_time%60
        minute = secs//60

        return " " + str(minute) + ":" + str(secs)