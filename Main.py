import pygame
import logging
import Sodoku
import GUI
import numpy as np

def redraw_window(win, board, new_btn, solve_btn, play_time, strikes):
    win.fill((255,255,255))
    
    board.draw()
    new_btn.draw()
    solve_btn.draw()
    
    fnt = pygame.font.SysFont("comicsans", 40)
    txt = fnt.render("Time: " + play_time, 1, (0, 0, 0))
    win.blit(txt, (540 - 180, 550))

    txt = fnt.render("X " * strikes, 1, (255, 0, 0))
    win.blit(txt, (540 - txt.get_width() - 5, 660 - 10 - txt.get_height()))

def main():
    win = pygame.display.set_mode((540, 660))
    pygame.display.set_caption("The Sodoku Demo")
    puzzle = Sodoku.Sodoku()
    puzzle.make_grid()
    puzzle.make_puzzle()
    board = GUI.Grid(0, 0, 540, 540, win, puzzle)
    new_btn = GUI.Button(10, 550, 80, 40, win, 540, 660, "New")
    solve_btn = GUI.Button(10, 610, 80, 40, win, 540, 660, "Solve")
    key = None
    run = True
    strikes = 0
    time = GUI.TimeStamp()
        
    while run:
        # print("loop count is " + str(loop_count))
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1 or event.key == pygame.K_KP1:
                    print("1 was pressed")
                    if board.selected:
                        board.set_selected_temp(1)
                if event.key == pygame.K_2 or event.key == pygame.K_KP2:
                    print("2 was pressed")
                    if board.selected:
                        board.set_selected_temp(2)
                if event.key == pygame.K_3 or event.key == pygame.K_KP3:
                    print("3 was pressed")
                    if board.selected:
                        board.set_selected_temp(3)
                if event.key == pygame.K_4 or event.key == pygame.K_KP4:
                    print("4 was pressed")
                    if board.selected:
                        board.set_selected_temp(4)
                if event.key == pygame.K_5 or event.key == pygame.K_KP5:
                    print("5 was pressed")
                    if board.selected:
                        board.set_selected_temp(5)
                if event.key == pygame.K_6 or event.key == pygame.K_KP6:
                    print("6 was pressed")
                    if board.selected:
                        board.set_selected_temp(6)
                if event.key == pygame.K_7 or event.key == pygame.K_KP7:
                    print("7 was pressed")
                    if board.selected:
                        board.set_selected_temp(7)
                if event.key == pygame.K_8 or event.key == pygame.K_KP8:
                    print("8 was pressed")
                    if board.selected:
                        board.set_selected_temp(8)
                if event.key == pygame.K_9 or event.key == pygame.K_KP9:
                    print("9 was pressed")
                    if board.selected:
                        board.set_selected_temp(9)
                if event.key == pygame.K_DELETE or event.key == pygame.K_BACKSPACE:
                    print("delete or backspace was pressed")
                    if board.selected:
                        board.clear_selected()

                if (event.key == pygame.K_RETURN or event.key == pygame.K_KP_ENTER) and board.selected:
                    print("enter was pressed")
                    row, col = board.selected
                    temp = board.board[col][row].temp
                    if temp != 0 or not(board.finished):
                        if board.set_value_at(col, row, temp):
                            print("success")
                        else:
                            print("wrong")
                            strikes += 1
                            if strikes > 5:
                                print("you lose")
                                run = False            #TODO: set proper fail state instead of just quiting

                        if puzzle.is_solved():
                            print("you win")
                            board.set_finished(True)
                            board.board[col][row].set_selected(False)
                
            elif event.type == pygame.MOUSEBUTTONDOWN:
                print("mouse clicked")
                mouse_pos = pygame.mouse.get_pos()
                print("mouse pos" + str(mouse_pos))
                square_pos = board.get_square_at_pos(mouse_pos)
                click_status_solve = solve_btn.update_state(mouse_pos, True)
                click_status_new = new_btn.update_state(mouse_pos, True)
                if square_pos:
                    board.select_square(square_pos)
                elif click_status_new:

                    board.new_game()
                    time.reset()

                elif click_status_solve:
                    board.animated_solve()
            else:
                mouse_pos = pygame.mouse.get_pos()
                solve_btn.update_state(mouse_pos, False)
                new_btn.update_state(mouse_pos, False)

        redraw_window(win, board, new_btn, solve_btn, time.get_timestring(), strikes)
        pygame.display.update()                            

main()
pygame.quit()